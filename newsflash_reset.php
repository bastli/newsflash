<?php

$list_filename = realpath("../Newsflash-Verteiler");

$email = $_REQUEST['email'];
if (!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL))
	die("Non-valid email format.");

$list = file_get_contents($list_filename);

if (!preg_match("/^$email\$/m", $list))
        die("Email address not in mailing-list.");

$list = preg_replace("/\n$email(\n|\$)/s", "\n", $list);

file_put_contents($list_filename, $list);

echo "Email address $email successfully unsubcribed from mailing-list.";

?> 
