<?php

$list_filename = realpath("../Newsflash-Verteiler");

$email = $_REQUEST['email'];
if (!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL))
	die("Non-valid email format.");

$list = file_get_contents($list_filename);

if (preg_match_all("/^$email\$/m", $list))
	die("Email address already in mailing-list.");

file_put_contents($list_filename, $email . "\n", FILE_APPEND);

echo "Email address $email successfully subscribed.";

?>