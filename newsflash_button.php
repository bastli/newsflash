<!-- ADD JQUERY 1.9 or 2+ TO YOUR HTML HEAD TO ALLOW FOR JAVASCRIPT CALLS -->

<div class="blue">
    <form
            class="newsflash-form" 
            action="newsflash_register.php" 
            method="post" 
            onsubmit="
                $.get($('.newsletter-form').attr('action'), $('.newsletter-form').serialize(), function(){
                    $('.newsletter-form h3').text('Registriert ^^');
                }); 
                return false;
            "
    >
        <h3>Hier eintragen …</h3>
        <input class="email" type="email" name="email" placeholder="Email" />
        <button class="subscribe" type="submit">Abonnieren</button>
        <button 
            class="unsubscribe" 
            onclick="
                $.get('newsflash_unregister.php', $('.newsletter-form').serialize(), function(){
                    $('.newsletter-form h3').text('Abgemeldet ;(');
                }); 
                return false;
            "
        >
            wieder abbestellen
        </button>
    </form>
    <h2>Newsletter</h2>
    <p>Der Bastli bietet einen Newsletter per E-Mail. Der Newsletter geht ca. einmal pro Monat raus und hält euch somit auf dem Laufenden.</p>
    <p>Es gibt <strong>kein</strong> lästiges Bestätigungsmail beim Anmelden.</p>
</div>